#!/usr/bin/env python3

import sys
import argparse

from pdfrw import PdfReader, PdfWriter, PageMerge

def iter_chunks(data, dim):
    for i in range(0, len(data), dim):
        yield data[i:i+dim]

def iter_concat(iterables):
    for iterable in iterables:
        for cur in iterable:
            yield cur

def inscribe_rect(inside, outside): # rect = (x, y, w, h)
    scale = min(a/b for a, b in zip(outside, inside))
    padding = tuple((a-b*scale)/2 for a, b in zip(outside, inside))
    return (scale, padding)

class PdfTailor:
    def __init__(self):
        self._pieces = []
        self._output = PdfWriter()
    
    def add_pages(self, path, *, pages=None):
        source = PdfReader(path)
        if pages is None: pages = range(source.numPages)
        
        for p in pages:
            self._pieces.append(source.pages[p])
    
    def cut_pages(self, rects):
        res = []
        for p in self._pieces:
            for r in rects:
                tmp = PageMerge()
                tmp.add(p, viewrect=r)
                res.append(tmp.render())
        self._pieces = res
    
    def sew_pages(self, rects, *, page_size=None):
        res = []
        for chunk in iter_chunks(self._pieces, len(rects)):
            tmp = PageMerge() + chunk
            for rect, page in zip(rects, tmp):
                x, y, w, h = rect
                (pad_x, pad_y), scale = inscribe_rect(page.BBox, (w, h))
                page.scale(scale)
                page.x = x + pad_x
                page.y = y + pad_y
            tmp = tmp.render()
            if page_size: tmp.MediaBox = page_size
            res.append(tmp)
        self._pieces = res
    
    def commit(self):
        for p in self._pieces:
            self._output.addpage(p)
        
        self._pieces = []
    
    def write(self, out_path):
        if self._pieces:
            self.commit()
        self._output.write(out_path)

def default_parser():
    def int_range(x): # 'xx-yy' -> (xx, xx+1, ..., yy)
        start, end = (tmp := x.split('-')[-2:])[0], tmp[-1]
        step = 1 if start <= end else -1
        return range(int(start), int(end)+step, step)
    
    def int_rect(x):
        x, y, w, h = (tmp := x.split(':')[-4:])[0], tmp[1], tmp[2], tmp[3]
        return (int(x), int(y), int(w), int(h))
    
    parser = argparse.ArgumentParser(description='Utility for PDF cutting and sewing.')
    parser.add_argument('-i','--input', type=str, required=True,
        help='Input file.')
    parser.add_argument('-p', '--pages', type=int_range, nargs='+',
        help='Select pages from input (page number N or inclusive ranges START-END).')
    parser.add_argument('-c', '--cut', type=int_rect, nargs='+',
        help='Rectagle regions to cut (x:y:w:h).')
    parser.add_argument('-s', '--sew', type=int_rect, nargs='+',
        help='Rect(s) used to stitch pages (x:y:w:h).')
    parser.add_argument('-o', '--output', type=str, default='./out.pdf',
        help='Output file.')
    return parser

if __name__ == '__main__':
    parser = default_parser()
    args = parser.parse_args()
    
    if args.pages:
        args.pages = iter_concat(args.pages)
    
    tailor = PdfTailor()
    tailor.add_pages(args.input, pages=args.pages)
    
    if args.cut: tailor.cut_pages(args.cut)
    if args.sew: tailor.sew_pages(args.sew)
    
    tailor.write(args.output)
    
