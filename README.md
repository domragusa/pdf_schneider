# PDF Schneider

A python utility to cut and sew back together pdf pages.

## Dependencies

### pdfrw

On Debian and derivatives (Ubuntu, Mint, etc):

```console
$ apt-get install python3-pdfrw
```

On Arch:

```console
$ pacman -S python-pdfrw
```

## Installation

Copy all the files in a nice directory (ex. ~/.local/lib/pdf_schneider/), make the script executable and finally put a link to the script in a directory included in your $PATH (ex. ~/.local/bin).

Like so:

```console
$ mkdir -p ~/.local/lib/pdf_schneider/
$ git clone https://gitlab.com/drtiny/pdf_schneider.git
$ cp ./pdf_schneider/* ~/.local/lib/pdf_schneider/
$ chmod +x ~/.local/lib/pdf_schneider/main.py
$ mkdir -p ~/.local/bin/
$ ln -s ~/.local/lib/pdf_schneider/main.py ~/.local/bin/pdf_schneider
```

## Usage

Reference the help:

```console
$ pdf_schneider -h
usage: pdf_schneider [-h] -i INPUT [-p PAGES [PAGES ...]] [-c CUT [CUT ...]]
               [-s SEW [SEW ...]] [-o OUTPUT]

Utility for PDF cutting and sewing.

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        Input file.
  -p PAGES [PAGES ...], --pages PAGES [PAGES ...]
                        Select pages from input (page number N or inclusive
                        ranges START-END).
  -c CUT [CUT ...], --cut CUT [CUT ...]
                        Rectagle regions to cut (x:y:w:h).
  -s SEW [SEW ...], --sew SEW [SEW ...]
                        Rect(s) used to stitch pages (x:y:w:h).
  -o OUTPUT, --output OUTPUT
                        Output file.

```
